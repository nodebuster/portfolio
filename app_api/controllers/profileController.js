(function(){
  'use strict';
})();

var cors = require('cors');
var mongoose = require('mongoose');
/* require datastructure (data model) */
var ProfileModel = require('../models/profileModel');
/* Use ES6 promise instead of deprecated mongoose promise; */
mongoose.Promise = global.Promise;
/* connect to database */
mongoose.connect('mongodb://localhost/profiledb');

/* A reusable function for sending json response to request */
var sendJsonResponse = function (res, status, content) {
  res.status(status);
  res.json(content); // res.jsonsend(content);
};

/* GET */
module.exports.profileList = function (req, res) {
  ProfileModel
    .find({})
    .exec(
      function(err, profile){ // profile tarkoittaa tässä palautettavaa dataa
        if (err) {  
          sendJsonResponse(res, 404, err);
          return;
        } else {
          sendJsonResponse(res, 200, profile);
        }
      });
};

/* GET one byId */
module.exports.profileReadOne = function (req, res) {
  ProfileModel
    .findById(req.params.profileid) // määritelty routessa /profiles/:profileid
    .exec(
      function(err, profile){
        if (err) {
          sendJsonResponse(res, 404, err);
          return;
        } else {
          sendJsonResponse(res, 200, profile);
        }
      });
};

/* POST */
module.exports.profileWrite = function (req, res) {
  ProfileModel
    .create({
      id : req.body.id,
      name : req.body.name,
      age : req.body.age,
      city : req.body.city
    }, function (err, profile) {
      if (err) {
        sendJsonResponse(res, 400, err);
        return;
      } else {
        sendJsonResponse(res, 201, profile);
      }
    });   
};

/* PUT (Update) todo: needs further inspection */
/* module.exports.profileUpdate = function (req, res) {
  ProfileModel 
    .findById(req.params.profileid)
    .exec(
      function (err, profile) {
        profile.name = req.body.name;
        profile.save(function(err, profile) {
          if (err) {
            sendJsonResponse(res, 404, err);
            return;
          } else {
            sendJsonResponse(res, 200, profile);
          }
        });
      }
    );
}; */


module.exports.profileUpdateAll = function (req, res) {
  ProfileModel 
    .findById(req.params.profileid)
    .exec(
      function (err, profile) {

        profile.id = req.body.id;
        if (req.body.name.toString().length > 5 && req.body.name.toString().length < 20) profile.name = req.body.name; else { profile.name = "Name is too short"; }
        profile.age = req.body.age;
        profile.city = req.body.city;

        profile.save(function(err, profile) {
          if (err) {
            sendJsonResponse(res, 404, err);
            return;
          } else {
            sendJsonResponse(res, 200, profile);
          }
        });
      }
    );
};


/* DELETE (one) */
module.exports.profileDeleteOne = function (req, res) {
  var profileid = req.params.profileid;
  if (profileid) {
    ProfileModel
      .findByIdAndRemove(profileid)
      .exec(
        function (err, profile) {
          if (err) {
            sendJsonResponse(res, 404, err);
            return;
          } 
          sendJsonResponse(res, 204, null);
        }
      );
  } else {
    sendJsonResponse(res, 404, {
      'message' : profileid
    });
  }
};
