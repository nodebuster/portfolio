var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var profileSchema = new Schema({
    id: String,
    name: String,
    age: Number,
    city: String
  });

var ProfileModel = mongoose.model('ProfileModel', profileSchema);

module.exports = ProfileModel;
