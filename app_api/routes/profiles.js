var express = require('express');
var router = express.Router();
var profileController = require('../controllers/profileController');

/* Profile CRUD methods */
router.get('/profiles', profileController.profileList);
router.post('/profiles', profileController.profileWrite);
router.get('/profiles/:profileid', profileController.profileReadOne);
// router.put('/profiles/:profileid', profileController.profileUpdate);
router.put('/profiles/:profileid', profileController.profileUpdateAll);
router.delete('/profiles/:profileid', profileController.profileDeleteOne);

module.exports = router;


